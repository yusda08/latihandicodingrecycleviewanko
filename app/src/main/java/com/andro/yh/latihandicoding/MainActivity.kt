package com.andro.yh.latihandicoding

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.andro.yh.latihandicoding.adapter.AdapterClub
import com.andro.yh.latihandicoding.model.ModelClub
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.startActivity

class MainActivity : AppCompatActivity() {

    private var items: MutableList<ModelClub> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initData()
        club_list.layoutManager = LinearLayoutManager(this)
        club_list.adapter = AdapterClub(this, items) {
            startActivity<DetailActivity>(
                "NAME" to it.name,
                "KET" to it.ket,
                "IMAGE" to it.image
            )

        }
    }

    private fun initData(){
        val name = resources.getStringArray(R.array.club_name)
        val ket = resources.getStringArray(R.array.club_ket)
        val image = resources.obtainTypedArray(R.array.club_image)
        items.clear()
        for(i in name.indices){
            items.add(ModelClub(name[i], ket[i], image.getResourceId(i, 0)))
        }
        image.recycle()
    }
}
