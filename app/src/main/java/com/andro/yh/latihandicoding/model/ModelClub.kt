package com.andro.yh.latihandicoding.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ModelClub (val name: String?, val ket:String?, val image: Int?): Parcelable