package com.andro.yh.latihandicoding

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.squareup.picasso.Picasso
import org.jetbrains.anko.*

class DetailActivity : AppCompatActivity(){
    private var nameClub : String = ""
    private var ketClub : String = ""
    private var imageClub : Int = 0

    private lateinit var name : TextView
    private lateinit var ket : TextView
    private lateinit var image : ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        linearLayout{
        lparams(width = matchParent, height = wrapContent)
        orientation = LinearLayout.VERTICAL
            padding = dip(16)

            image = imageView{

            }.lparams{
                width = dip(100)
                height = dip(100)
                gravity = Gravity.CENTER
            }

            name = textView()
            ket = textView()
        }

        val i = intent
        nameClub = i.getStringExtra("NAME")
        ketClub = i.getStringExtra("KET")
        imageClub = i.getIntExtra("IMAGE", 0)


        name.text = nameClub
        ket.text = ketClub
        Picasso.get().load(imageClub).into(image)

    }

}