package com.andro.yh.latihandicoding.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.andro.yh.latihandicoding.R
import com.andro.yh.latihandicoding.R.id.*
import com.andro.yh.latihandicoding.model.ModelClub
import com.squareup.picasso.Picasso
import kotlinx.android.extensions.LayoutContainer
import org.jetbrains.anko.*


class AdapterClub(private val context: Context, private  val items: List<ModelClub>, private val listener: (ModelClub)->Unit)
    : RecyclerView.Adapter<AdapterClub.ViewHolder>(){
    class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
        LayoutContainer {
        private val name: TextView = containerView.find(name_club)
        private val image: ImageView = containerView.find(image_club)
        fun bindItem(items: ModelClub, listener: (ModelClub) -> Unit){
            name.text = items.name
            items.image?.let { Picasso.get().load(it).into(image) }

            containerView.setOnClickListener{
                listener(items)
            }
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(ClubUI().createView(AnkoContext.create(p0.context, p0)))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.bindItem(items[p1], listener)
    }



}

class ClubUI : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            linearLayout {
                lparams(width = matchParent, height = wrapContent)
                padding = dip(5)
                orientation = LinearLayout.HORIZONTAL

                imageView {
                    id = R.id.image_club
                }.lparams {
                    width = dip(50)
                    height = dip(50)
                }

                textView {
                    id = R.id.name_club
                    textSize = 16f
                }.lparams {
                    width = matchParent
                    height = wrapContent
                    margin = dip(10)
                    gravity = Gravity.CENTER_VERTICAL
                }


            }
        }

    }
}
